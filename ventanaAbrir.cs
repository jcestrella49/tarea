﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Tutorial
{
    public partial class ventanaAbrir : Form
    {
        public string x;
        

        public int opcion=0;
        public ventanaAbrir()
        {
            InitializeComponent();
            //leerArchivo = new StreamReader("C:\\Users\\Druko\\Desktop\\5to Semestre\\MiTexto.txt");
            
        }

        private void ventanaAbrir_Load(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        

        private void cargar_Click(object sender, EventArgs e)
        {

            //ventanaAbrir nueva = new ventanaAbrir();
            //this.Hide();
            //nueva.Show();
            //"^[a-z|A-Z]{7,}$"
            if (opcion == 1)
            {
                leyendo();
                string exp = "^[a-z|A-Z]{7,}$";
                procesoExpreciones(exp);
                
            }
            if (opcion == 2)
            {
                leyendo();
                string exp = ".*([B-D]|[b-d]|[F-H]|[f-h]|[J-N]|[j-n]|[P-T]|[p-t]|[V-Z]|[v-z])+$";
                procesoExpreciones(exp);
            }
            if (opcion == 3)
            {
                leyendo();
                string exp = "^[Mm]([B-D]|[b-d]|[F-H]|[f-h]|[J-N]|[j-n]|[P-T]|[p-t]|[V-Z]|[v-z]).*";
                procesoExpreciones(exp);
            }
            if (opcion == 4)
            {
                leyendo();
                string texto = x;
                string[] cadenas = Regex.Split(texto, " ");
                string exp = "('.*')|(\".*\")*";
                // [a-z|A-Z]{1,}e[\s|.]
                string agregar = "";
                for (int i = 0; i < cadenas.Length; i++)
                {
                    MatchCollection encontrado = Regex.Matches(cadenas[i], exp);
                    foreach (Match res in encontrado)
                    {
                        agregar += string.Join(";", res);
                        //agregar += "\n";
                        //var toarray = res;
                        //string newflatChain = string.Join(";", toarray);
                    }
                }
                cajaTexto.Text = agregar;
            }
            if (opcion == 5)
            {
                leyendo();
                string exp = ".*(\\d+\\.\\d+\\.\\d+).*";
                procesoExpreciones(exp);
            }
            if (opcion == 6)
            {
                leyendo();
                string exp = "^.*([0-3][0-9].[0-1][0-9].[0-9][0-9]).*$";
                procesoExpreciones(exp);
            }
            if (opcion == 7)
            {
                leyendo();
                string exp = "(^9[0-9]{9})";
                procesoExpreciones(exp);
            }
            if (opcion == 8)
            {
                leyendo();
                string exp = "^[a-zA-Z0-9_\\-\\.~]{2,}@[a-zA-Z0-9_\\-\\.~]{2,}\\.[a-zA-Z]{2,4}$";
                procesoExpreciones(exp);
            }
            if (opcion == 9)
            {
                leyendo();
                string exp = "^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$";
                //"^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$"
                procesoExpreciones(exp);
            }
            if (opcion == 10)
            {
                leyendo();
                string exp = "^[0-9]{5}$";
                procesoExpreciones(exp);
            }
        }
        public void leyendo()
        {
            using (StreamReader leer = new StreamReader(@"C:\\Users\\Druko\\Desktop\\5to Semestre\\MiTexto.txt"))
            {
                while (!leer.EndOfStream)
                {
                    x = leer.ReadLine();
                }
            }
        }
        public void procesoExpreciones(String e)
        {
            string texto = x;
            string[] cadenas = Regex.Split(texto, " ");
            string agregar = "";
            for (int i = 0; i < cadenas.Length; i++)
            {
                MatchCollection encontrado = Regex.Matches(cadenas[i], e);
                foreach (Match res in encontrado)
                {
                    agregar += string.Join(";", res);
                    agregar += "\n";
                    //var toarray = res;
                    //string newflatChain = string.Join(";", toarray);
                }
            }
            cajaTexto.Text = agregar;
        }
    }
}
