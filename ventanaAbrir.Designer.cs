﻿namespace Tutorial
{
    partial class ventanaAbrir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cajaTexto = new System.Windows.Forms.RichTextBox();
            this.cargar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cajaTexto
            // 
            this.cajaTexto.Location = new System.Drawing.Point(12, 12);
            this.cajaTexto.Name = "cajaTexto";
            this.cajaTexto.Size = new System.Drawing.Size(618, 305);
            this.cajaTexto.TabIndex = 0;
            this.cajaTexto.Text = "";
            this.cajaTexto.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // cargar
            // 
            this.cargar.Location = new System.Drawing.Point(280, 323);
            this.cargar.Name = "cargar";
            this.cargar.Size = new System.Drawing.Size(75, 30);
            this.cargar.TabIndex = 1;
            this.cargar.Text = "Cargar";
            this.cargar.UseVisualStyleBackColor = true;
            this.cargar.Click += new System.EventHandler(this.cargar_Click);
            // 
            // ventanaAbrir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 359);
            this.Controls.Add(this.cargar);
            this.Controls.Add(this.cajaTexto);
            this.Name = "ventanaAbrir";
            this.Text = "Busqueda";
            this.Load += new System.EventHandler(this.ventanaAbrir_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox cajaTexto;
        private System.Windows.Forms.Button cargar;
    }
}